import {createContext ,useReducer} from 'react'
export const categoriContext =createContext()

const initialState ={
    category:[]
}
function reducer(state , action) {
    switch (action.type) {
        case ADD-ITEMS:
            const newItem = [ ...state.category,action.payload]
          
            return { ...state,category: newItem }
                
       
    }
}
// در ابتداس پروزه  در _app <CategoryProvider>  رو رپ میکنیم
// در هر جا که بخواهیم استفاده کنیم  ابتدا 
// import {useContext} from 'react'
//import {categoriContext} from 'از آدرس این فایل'
// const {state,dispatch } useContext(categoriContext)

export function CategoryProvider({children}){
    const [state ,dispatch] =useReducer(reducer .initialState)
    const value={state,dispatch}
    return (<categoriContext.Provider value={value}>
        {children}
        </categoriContext.Provider>)
}
const [state, dispatch] = useReducer(reducer ,initialState)