import bcrypt from 'bcryptjs';

const user = [
    {
        name: "user1",
        email: "jwwj@kjskjkd",
        password: bcrypt.hashSync('123456') ,
        isAdmin: true
    }, {
        name: "user2",
        email: "jwwj@kjskjkd",
        password: bcrypt.hashSync('123456') ,
        isAdmin: false
    }
]
export default user
