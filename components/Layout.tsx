import { Box, Drawer, IconButton, List, ListItem, ListItemButton, Paper, Typography } from "@mui/material";
import Head from "next/head";
import Link from "next/link";
import { useState } from "react";
import { useTheme } from "@emotion/react";
import { signOut, useSession } from "next-auth/react";
import { Title } from "@mui/icons-material";
import { ToastContainer } from "react-toastify";
const drawerWidth = 240;

function Layout({ children, title ,window}: any) {
  const { status, data: session } = useSession()

  const [mobileOpen, setMobileOpen] = useState(true);
  const [open, setOpen] = useState(false);
  const theme = useTheme();

  const container = window !== undefined ? () => window().document.body : undefined;

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const logoutHandler = () => {
    signOut({callbackUrl:'/login'})
  }
  console.log("session" ,session);
  

  return (
    <>
      <Head>
        <title>{`${title}`}</title>
      </Head>
      <ToastContainer position='bottom-center' limit={1} />
      <div className="flex min-h-screen flex-col justify-between">
        <header>
          <nav className="flex h-14 px-8 justify-between items-center border-b-4 bg-white">
            <Link href="/">
              <a className="text-lg font-bold">Admin panel</a>
            </Link> 
            <Link href="/category/add">
              <a className="p-2">Category Add</a>
            </Link> 
            <Link href="/category">
              <a className="p-2">Category List</a>
            </Link> 
            {
              status === 'loading' ? ('loading'):
              session?.user ? (<>
              <div>{session.user.name}</div>
              <a className="flex p-2" href="#" onClick={logoutHandler}>
                logout
              </a>
              </>):(<Link href='/login'>
                  <a className='p-2'>Login</a>
                </Link>)
            }
          </nav>
        </header>
       
        <main className="container bg-slate-400 justify-between m-auto mt-4 ">
          <Paper  > 
          <Typography className="px-5 py-2 bg-slate-400" variant="h4" gutterBottom>
            {title}
          </Typography>
          <Paper>
            <div className=" "> {children}</div>
          </Paper>
        
          </Paper>
        </main>
        <footer className="flex justify-center items-center h-10 bg-slate-400">
          Footer
        </footer>
      </div>
    </>
  );
  {
  }
}

export default Layout;
