import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { IconButton } from '@mui/material';

export default function CardIems(props:any) {
  return (
    <Card sx={{ width: 250 }} className='m-5'>
      <CardMedia
        sx={{ height: 120 }}
        image="/images/draw2.webp"
        title={props.title}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.title}
        </Typography>
        {/* <Typography variant="body2" color="text.secondary"></Typography> */}
      </CardContent>
      {props.cardActions}
    </Card>
  );
}