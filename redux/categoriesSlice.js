import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    categoryItems: [],
    meetingItems: [],
    categoryId:'',
    meetingId:''
}

const categoriesSlice = createSlice({
    name:'category',
    initialState,
    reducers: {
    addToCategory:(state ,action)=>{
        state.categoryItems=action.payload
    }, 
    addTomeeting:(state ,action)=>{
        state.meetingItems=action.payload
    },
    setMeetingId:(state ,action)=>{
        state.meetingId =action.payload
    },
    setCategoryId:(state ,action)=>{
        state.categoryId =action.payload
    },
}
   
})

export const {addToCategory ,addTomeeting ,setCategoryId ,setMeetingId} = categoriesSlice.actions

export default categoriesSlice.reducer
