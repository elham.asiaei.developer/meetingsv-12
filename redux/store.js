import {configureStore} from '@reduxjs/toolkit';

import categoriesSliceReducer from './categoriesSlice';

export const store = configureStore({
    reducer:{
        category:categoriesSliceReducer, 
    }

})