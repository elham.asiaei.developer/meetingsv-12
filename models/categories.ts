import mongoose from 'mongoose';

const categoriesSchema = new mongoose.Schema({
    id:{type:String , required:false },
    title:{type:String ,required:true},
   })

const Categories = mongoose.models.Categories || mongoose.model('Categories', categoriesSchema )

export default Categories