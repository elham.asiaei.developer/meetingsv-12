import mongoose from 'mongoose';

const meetingsSchema = new mongoose.Schema({
    id:{type:String , required:false },
    categoryId:{type:String , required:true },
    title:{type:String ,required:true},
   })

const Categories = mongoose.models.Meetings || mongoose.model('Meetings', meetingsSchema )

export default Categories