import mongoose from 'mongoose';

const detailsSchema = new mongoose.Schema({
    id:{type:String , required:true },
    categoryId:{type:String , required:true },
    meetingId:{type:String , required:true },
    title:{type:String ,required:true},
    path:{type:String ,required:true},
   })

const Categories = mongoose.models.Details || mongoose.model('Details', detailsSchema )

export default Categories