import mongoose from 'mongoose';

async function connect(){
   await mongoose.connect('mongodb://127.0.0.1:27017/meetings')
    console.log('Connected.');
    
}

function convertToObj (doc:any) {
    doc._id =doc._id.toString()

    return doc
}

const db={connect ,convertToObj}
export default db