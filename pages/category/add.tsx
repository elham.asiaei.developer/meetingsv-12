import { Button, Input } from '@mui/material';
import Layout from '../../components/Layout'
import React, { useRef, useLayoutEffect } from 'react';
import { Label } from '@mui/icons-material';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import {toast} from 'react-toastify';
import { useRouter } from 'next/router'

interface event{

}

function Add() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const router = useRouter()
 const titleInptRef =useRef<HTMLInputElement | null>(null)
 const pathInptRef =useRef<HTMLInputElement | null>(null)

 async function submitHandler(values:any) {
  try{
     const result = await axios.post('/api/categories',{...values ,id:2})

    toast.success("Succsessfully added")  
    router.push('/category')  
  }
  catch (err) {
    console.log(err)
  }
}
 
  return (
    <Layout title="Add Category">
      <form className="p-4" onSubmit={handleSubmit(submitHandler)}>
        <Input
          {...register("title", { required: true })}
          type="text"
          className=" rounded-xl p-2 outline-0 m-4"
          id="title"
          placeholder="title"
          autoFocus
          multiline={true}
        />
        {errors.title && <div className="text-red-500">Please Enter Email</div>}
        <Button
          type="submit"
          className="rouunded-xl bg-blue-700 text-white px-4 py-2 w-28"
        >
          add
        </Button>
      </form>
    </Layout>
  );
}


export default Add
