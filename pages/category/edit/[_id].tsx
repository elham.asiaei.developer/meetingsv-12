import { useForm } from 'react-hook-form';
import Categories from '../../../models/categories'
import db from "../../../utils/db";
import React from 'react'
import { useRouter } from 'next/router';
import axios from 'axios';
import {toast} from 'react-toastify';
import Layout from '../../../components/Layout';
import { Button, Input } from '@mui/material';

type categoryType ={
  category:{
    _id:string;
    title:string;
    id?:string
  }
}

export default function (category:categoryType) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues:{
      title:category.category.title
    }
  });

  const router = useRouter()

 async function submitHandler({ title}:any) {
  try{
     const result = await axios.put('/api/categories',{...category,title})
       toast.success("Succsessfully added")  
        router.push('/category')  
  }
  catch (err) {
    console.log(err)
  }
}
console.log('category', category)
   return (
    <Layout title="Add Category">
    <form className="p-4" onSubmit={handleSubmit(submitHandler)}>
      <Input
        {...register("title", { required: true })}
        type="text"
        className=" rounded-xl p-2 outline-0 m-4"
        id="title"
        placeholder="title"
        autoFocus
        multiline={true}
      />
      {errors.title && <div className="text-red-500">Please Enter Email</div>}
      <Button
        type="submit"
        className="rouunded-xl bg-blue-700 text-white px-4 py-2 w-28"
      >
        add
      </Button>
    </form>
  </Layout>)
}

export async function getServerSideProps(context:any) {
    const { params } = context
    const { _id } = params
    await db.connect()
  
    const category = await Categories.findOne({ _id }).lean()
  
    return {
      props: {
        category: category ? db.convertToObj(category) : null,
      },
    }
  }
  