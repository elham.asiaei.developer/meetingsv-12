import Link from "next/link";
import Layout from "../../components/Layout";
import AlertDialogSlide from "../../components/dialog";
import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardActions,
  IconButton,
  Typography,
} from "@mui/material";
import db from "../../utils/db";
import Categories from "../../models/categories";
import CardIems from "../../components/cardIems";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import axios from "axios";
import { useDispatch } from "react-redux";
import { addToCategory } from "../../redux/categoriesSlice";
import VisibilitySharpIcon from '@mui/icons-material/VisibilitySharp';

export default function Index(props: any) {
  const { categories } = props;
  const dispatch = useDispatch();

  async function handleShow() {
    const response = await fetch("/api/categories");
    const responseData = await response.json();
  }

  useEffect(() => {
    dispatch(addToCategory(categories));
  }, [categories]);

  async function deleteCategory(e: any, id: any) {
    e.persist();
    console.log("id :>> ", id);
    await axios.delete(`/api/categories/${id}`);
  }

  return (
    <Layout title="list">
      <div className="flex flex-wrap items-start justify-start px-4">
        <Link href="/category/add">
          <AddIcon color="primary" className=" text-2xl" />
        </Link>
      </div> 
      <div className="flex flex-wrap items-center justify-center">
        {categories?.map((category: any) => (
          <Link key={category?._id} href={`/meetings/${category._id}`}>
            {
              <CardIems
                key={category?._id}
                title={category.title}
                cardActions={
                  <CardActions className="flex flex-col-2">
                    <IconButton
                      onClick={(e) => deleteCategory(e, category._id)}
                      aria-label="delete"
                      color="error"
                      size="large"
                    >
                      <DeleteIcon />
                    </IconButton>
                    <Link
                      key={category?._id}
                      href={`/category/edit/${category._id}`}
                    >
                      <IconButton
                        aria-label="edit"
                        color="primary"
                        size="large"
                      >
                        <EditIcon />
                      </IconButton>
                    </Link>
                    <Link
                      key={category?._id}
                      href={`/meetings/${category._id}`}
                    >
                      <IconButton
                        aria-label="edit"
                        color="primary"
                        size="large"
                      >
                        <VisibilitySharpIcon/>
                       
                      </IconButton>
                    </Link>
                  </CardActions>
                }
              />
            }
          </Link>
        ))}
      </div>
      <Button onClick={handleShow}> show</Button>
    </Layout>
  );
}
export async function getServerSideProps() {
  db.connect();
  const categoriesData = await Categories.find().lean();
  return { props: { categories: categoriesData.map(db.convertToObj) } };
}
