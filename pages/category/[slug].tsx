import React from "react";
import Layout from "../../components/Layout";
import { useRouter } from "next/router";
import db from "../../utils/db";
import Categories from "../../models/categories";

export default function CategoryDetails({category}:any) {
  const { query } = useRouter();
  const { slug } = query;
  if (!category) {
    return <div>Product not found.</div>
  }

  return (
    <>
      <Layout title="categoryDetail">
        CategoryDetails
        {slug}
        {category ? category?.title :"hhhhh"}

      </Layout>
    </>
  );
}

export async function getServerSideProps(context:any) {

 await db.connect();
  const { params } = context;
  const { slug } = params;
  console.log('slug server side', slug)
  const categoryData = await Categories.findOne({_id: slug }).lean();


  return { 
    props: {
       category: categoryData ? db.convertToObj(categoryData) : null } };
}
