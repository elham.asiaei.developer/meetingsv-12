import Link from 'next/link'
import { useRouter } from 'next/router'

import { useEffect } from 'react'

import { useForm } from 'react-hook-form'
import { signIn, useSession } from 'next-auth/react'


function LoginPage() {
  const { data: session } = useSession()

  const router = useRouter()
  const { redirect } = router.query

  useEffect(() => {
    if (session?.user) {
      router.push(redirect || '/')
    }
  }, [router, session, redirect])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  async function submitHandler({ email, password }) {
    try {
      const result = await signIn('credentials', {
        redirect: false,
        email,
        password,
      })

      if (result.error) {
        console.log('faild')
      }
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <section className="h-screen ">
              <div className="h-full">
              <div className="g-6 flex h-full flex-wrap items-stretch justify-center">
              <div className=" mb-12 grow-0 basis-auto md:mb-0 md:w-9/12  lg:w-6/12 xl:w-6/12">
            <img
              src="/images/draw2.webp"
              className="w-full"
              alt="Sample image"
            />
          </div>


                  <div className="mb-12  md:w-8/12 lg:w-6/12 xl:w-6/12 grid grid-cols-1 gap-4 content-center justify-items-center rtl">

      <form
className=" w-1/2 h-auto grid grid-cols-1 gap-4 content-center 
border-solid border-2 border-indigo-300 rounded-lg p-6 bg-gradient-to-r from-blue-400 to-blue-600 "        onSubmit={handleSubmit(submitHandler)}
      >
        <h2 className='mb-4 text-xl'>Login</h2>
        <div className='mb-4'>
          <input
            {...register('email', { required: true })}
            type='email'
            className='w-full rounded-xl p-2 outline-0'
            id='email'
            placeholder='Email'
            autoFocus
          />
          {errors.email && (
            <div className='text-red-500'>Please enter email.</div>
          )}
        </div>
        <div className='mb-4'>
          <input
            {...register('pasword', {
              required: true,
              minLength: {
                value: 5,
                message: 'Password mut be at least 5 chars.',
              },
            })}
            type='password'
            className='w-full rounded-xl p-2 outline-0'
            id='password'
            placeholder='Password'
            autoFocus
          />
          {errors.password && (
            <div className='text-red-500'>{errors.password.message}</div>
          )}
        </div>
        <div className='mb-4'>
          <button className='rounded-xl bg-gray-700 text-white px-4 py-2 w-28'>
            Login
          </button>
        </div>
        <div className='mb-4'>
          <Link href='register'>Register</Link>
        </div>
      </form>
      </div>
      </div>
      </div>
      </section>

  )
}

export default LoginPage
