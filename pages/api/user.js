import db from "../../utils/db";
import User from '../../models/user'
import userItem from '../../data/user'

async function handler(req ,res) {
    await db.connect()
    // await User.deleteMany()
    await User.insertMany(userItem)

    res.send({message:"userAdded!"})
}

export default handler