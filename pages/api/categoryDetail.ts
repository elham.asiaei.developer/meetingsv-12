import {MongoClient} from 'mongodb'

import Categories from '../../models/categories';

async function handler(req:any ,res:any){
    const client = await MongoClient.connect('mongodb://127.0.0.1:27017/test')
    if(req.method==='POST'){
      const {title,path} = req.body
      Categories.insertMany({title ,path})
      console.log('success')
    }
    else {
      
      const data = await Categories.find().toArray()
      res.json({data})

    }
}
export default handler