import db from "../../utils/db";
import Details from '../../models/details'
import DetailsItem from '../../data/details'

async function handler(req:any ,res:any) {
    await db.connect()
    await Details.deleteMany()
    await Details.insertMany(DetailsItem)

    res.send({message:"DetailsItem Added!"})
}

export default handler