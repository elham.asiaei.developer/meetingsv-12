import {MongoClient} from 'mongodb'

async function handler(req:any ,res:any){
  console.log('req', req)
    const client = await MongoClient.connect('mongodb://127.0.0.1:27017/test')
    if(req.method==='POST'){
      const {title,path} = req.body
      const db = await  client.db()
      db.collection('category').insertOne({title ,path})
      console.log('success')
    }
    else {
      
      const db = await  client.db()
      const data = await db.collection('category').find().toArray()
      res.json({data})

    }
}
export default handler