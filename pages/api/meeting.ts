import db from "../../utils/db";
import Meetings from '../../models/meetings'
import MeetingsItem from '../../data/meetings'

async function handler(req:any ,res:any) {
    await db.connect()
    await Meetings.deleteMany()
    await Meetings.insertMany(MeetingsItem)

    res.send({message:"userAdded!"})
}

export default handler