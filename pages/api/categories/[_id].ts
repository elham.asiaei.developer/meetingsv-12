import db from "../../../utils/db";
import Categories from '../../../models/categories'
import { useRouter } from 'next/router'

async function handler(req:any ,res:any) {
    const router =useRouter()
    console.log('router', router)
    // await db.connect()
console.log('req.method :>> ', req.method);
    switch (req.method) {
            case 'PUT':
            return update(); 
            case 'DELETE':
            return deleteUser();
            default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }

    async function update() {
        console.log('req.body', req.body)
        try {
            await Categories.findByIdAndUpdate(req.body._id,{title:req.body.title})

            return res.status(200).json({});
        } catch (error) {
            return res.status(400).json({ message: error });
        }
    }

    async function deleteUser() {
        try {
            const { id } = req.query;
            console.log('req.query :>> ', req.query);
            // await Categories.findOneAndDelete({_id:id })

            // return res.status(200).json({});
        } catch (error) {
            return res.status(400).json({ message: error });
        }
    }
  

}

export default handler