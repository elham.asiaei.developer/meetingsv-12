import Details from '../../../models/details'


async function handler(req:any ,res:any) {
    
console.log('req.method :>> ', req.method);
    switch (req.method) {
        case 'GET':
            return getUsers();
        case 'POST':
            return createUser();
            case 'PUT':
            return update(); 
            case 'DELETE':
            return deleteUser();
            default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }

     async function getUsers() {
        const details = await Details.find();
        return res.status(200).json(details);
    }
    
    async function createUser() {
        console.log('req.body', req.body)
        try {
            await Details.insertMany(req.body)

            return res.status(200).json({});
        } catch (error) {
            return res.status(400).json({ message: error });
        }
    } 
    async function update() {
        console.log('req.body', req.body)
        try {
            await Details.findByIdAndUpdate(req.body._id,{title:req.body.title})

            return res.status(200).json({});
        } catch (error) {
            return res.status(400).json({ message: error });
        }
    }

    async function deleteUser() {
        try {
        //   await const { id } = req.query;
            await console.log('req.query :>> ', req.query);
            // await Categories.findOneAndDelete({_id:id })

            // return res.status(200).json({});
        } catch (error) {
            return res.status(400).json({ message: error });
        }
    }
  

}

export default handler