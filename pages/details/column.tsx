import { GridColDef, GridRenderCellParams, GridValueGetterParams } from '@mui/x-data-grid';
import Link from 'next/link';

const columns  = [
    { field: '_id', headerName: 'ID', width: 200 },
    {
      field: 'meetingName',
      headerName: 'meeting name',
      width: 300,
      editable: true,
    },
    {
      field: 'title',
      headerName: 'title',
      width: 150,
      editable: true,
    },
    {
      field: 'path',
      headerName: 'path',
      width: 150,
      editable: true,
      renderCell: (params:any) => (
        <Link  href={`${params.row.path}`}>
        <a  className='decoration-double text-blue-600' target="_blank">{params.row.path} </a>
      </Link>
          )
     
    },
  ];

  export default columns
  