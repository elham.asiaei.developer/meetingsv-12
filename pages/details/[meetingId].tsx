import Link from "next/link";
import Layout from "../../components/Layout";
import AlertDialogSlide from "../../components/dialog";
import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Card,
  CardActions,
  IconButton,
  Typography,
} from "@mui/material";
import db from "../../utils/db";
import Details from "../../models/details";
import CardIems from "../../components/cardIems";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import axios from "axios";
import column from './column';
import { DataGrid } from "@mui/x-data-grid";
import { useRouter } from "next/router";
import { setMeetingId } from "../../redux/categoriesSlice";
import { useDispatch } from "react-redux";

export default function Index(props: any) {
  const { details } = props;
  const dispatch = useDispatch();
  const { query } = useRouter();
  const { meetingId } = query;

  useEffect(()=>{
    dispatch(setMeetingId(meetingId))
  },[details])

  if(!details){
    return <div>loading</div>
  }

  return (
    <Layout title="Add Details">
      <div className="flex flex-wrap items-start justify-start px-4">
        <Link href="/details/add">
          <AddIcon color="primary" className=" text-2xl" />
        </Link>
      </div>
      <div className="flex flex-wrap items-center justify-center">
      <Box sx={{ height: 400, width: '100%' }}>
        <DataGrid
        rows={details}
        columns={column}
        autoHeight
        checkboxSelection
      />
    </Box>
      
      </div>
    </Layout>
  );
}

export async function getServerSideProps(context:any) {
  const { params } = context
  const { meetingId } = params
  await db.connect()
   
  const detailsData = await Details.find({meetingId}).lean();
  return { props: { details: detailsData?detailsData.map(db.convertToObj):null } };
}

