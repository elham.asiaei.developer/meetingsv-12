import { Button, FormControl, Input, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import Layout from '../../components/Layout'
import React from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import {toast} from 'react-toastify';
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'

interface event{

}

function Add() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const router = useRouter()
  const meetingItems = useSelector((state:any) => state.category.meetingItems)
  const meetingId = useSelector((state:any) => state.category.meetingId)

 async function submitHandler(values:any) {
  try{
    console.log('values', values)
     const result = await axios.post('/api/details',{...values ,id:2 ,categoryId:2})
    toast.success("Succsessfully added")  
    router.push(`/details/${meetingId}`)  
  }
  catch (err) {
    console.log(err)
  }
}
 console.log('categoryItems', meetingItems)
  return (
    <Layout title="Add Details">
      <form className="flex p-4 m-2 content-center "  onSubmit={handleSubmit(submitHandler)}>
        <TextField
          {...register("meetingId", { required: true })}
          id="meetingId"
          name="meetingId"
          select
          label="meeting"
          helperText={errors.categoryId && <>Please select your currency</>}
          error={!!errors?.categoryId}
          autoFocus
          fullWidth={true}

        >
          {meetingItems.map((option: any) => (
            <MenuItem key={option._id} value={option._id}>
              {option.title}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          {...register("title", { required: true })}
          type="text"
          id="title"
          placeholder="title"
          label="title"
          autoFocus
          fullWidth={true}
          helperText={errors.title&& <>Please Enter Email</>}
          error={!!errors?.title}
        /> 
        <TextField
          {...register("path", { required: true })}
          type="text"
          id="path"
          placeholder="path"
          label="path"
          autoFocus
          fullWidth={true}
          helperText={errors.path&& <>Please Enter path</>}
          error={!!errors?.path}
        />
        <Button
          type="submit"
          className="rouunded-xl bg-blue-700 text-white px-4 py-2 w-28"
        >
          add
        </Button>
      </form>
    </Layout>
  );
}


export default Add
