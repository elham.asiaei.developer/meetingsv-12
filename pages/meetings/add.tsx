import { Button, FormControl, Input, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import Layout from '../../components/Layout'
import React, { useRef, useLayoutEffect } from 'react';
import { Label } from '@mui/icons-material';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import {toast} from 'react-toastify';
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'

interface event{

}

function Add() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const router = useRouter()
  const categoryItems = useSelector((state:any) => state.category.categoryItems)
  const categoryId = useSelector((state:any) => state.category.categoryId)

 async function submitHandler(values:any) {
  try{
    console.log('values', values)
     const result = await axios.post('/api/meetings',{...values ,id:2})
    toast.success("Succsessfully added")  
    router.push(`/meetings/${categoryId}`)  
  }
  catch (err) {
    console.log(err)
  }
}
 console.log('categoryItems', categoryItems)
  return (
    <Layout title="Add Meeting">
      <form className="flex p-4 m-2 content-center "  onSubmit={handleSubmit(submitHandler)}>
        <TextField
          {...register("categoryId", { required: true })}
          id="categoryId"
          name="categoryId"
          select
          label="category"
          helperText={errors.categoryId && <>Please select your currency</>}
          error={!!errors?.categoryId}
          autoFocus
          fullWidth={true}

        >
          {categoryItems.map((option: any) => (
            <MenuItem key={option._id} value={option._id}>
              {option.title}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          {...register("title", { required: true })}
          type="text"
          id="title"
          placeholder="title"
          label="title"
          autoFocus
          fullWidth={true}
          helperText={errors.title&& <>Please Enter Email</>}
          error={!!errors?.title}
        />
        <Button
          type="submit"
          className="rouunded-xl bg-blue-700 text-white px-4 py-2 w-28"
        >
          add
        </Button>
      </form>
    </Layout>
  );
}


export default Add
