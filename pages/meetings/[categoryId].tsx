import Link from "next/link";
import Layout from "../../components/Layout";
import AlertDialogSlide from "../../components/dialog";
import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardActions,
  IconButton,
  Typography,
} from "@mui/material";
import db from "../../utils/db";
import Meetings from "../../models/meetings";
import CardIems from "../../components/cardIems";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import axios from "axios";
import { useDispatch } from "react-redux";
import { addTomeeting, setCategoryId } from "../../redux/categoriesSlice";
import VisibilitySharpIcon from '@mui/icons-material/VisibilitySharp';
import { useRouter } from "next/router";

export default function Index(props: any) {
  const { meetings } = props;
  const {query}=useRouter()
  const {categoryId} =query

  const dispatch = useDispatch();

  console.log('meetings', meetings)

  useEffect(() => {
    dispatch(addTomeeting(meetings));
    dispatch(setCategoryId(categoryId))
  }, [meetings]);

  if(!meetings){
    return <div>loading</div>
  }

  return (
    <Layout title="list meeting">
      <div className="flex flex-wrap items-start justify-start px-4">
        <Link href="/meetings/add">
          <AddIcon color="primary" className=" text-2xl" />
        </Link>
      </div>
      <div className="flex flex-wrap items-center justify-center">
      
        {meetings?.map((category: any) => (
          <Link key={category?._id} href={`/details/${category._id}`}>
            {
              <CardIems
                key={category?._id}
                title={category.title}
                cardActions={
                  <CardActions className="flex flex-col-2">
                    <IconButton 
                    // onClick={(e)=>deleteCategory(e,category._id)} 
                    aria-label="delete" color="error" size="large">
                      <DeleteIcon />
                    </IconButton>
                    <Link
                      key={category?._id}
                      href={`/category/edit/${category._id}`}
                    >
                      <IconButton
                        aria-label="edit"
                        color="primary"
                        size="large"
                      >
                        <EditIcon />
                      </IconButton>
                    </Link>
                    <Link
                      key={category?._id}
                      href={`/details/${category._id}`}
                    >
                      <IconButton
                        aria-label="edit"
                        color="primary"
                        size="large"
                      >
                        <VisibilitySharpIcon/>
                        
                      </IconButton>
                    </Link>
                  </CardActions>
                }
              />
            }
          </Link>
        ))}
      </div>
      {/* <Button onClick={handleShow}> show</Button> */}
    </Layout>
  );
}

export async function getServerSideProps(context:any) {
  const { params } = context
  const { categoryId } = params
  await db.connect()
  
  const meetingsData = await Meetings.find({categoryId}).lean();
  return { props: { meetings: meetingsData?meetingsData.map(db.convertToObj):null } };
}

