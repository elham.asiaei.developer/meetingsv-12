import { Button, Input } from '@mui/material';
import Layout from '../components/Layout'
import { useRef, useState } from 'react';
import { Label } from '@mui/icons-material';
import axios from 'axios';
import {useSelector} from 'react-redux';
import Link from 'next/link';

function Home() {
const { categoryItems } = useSelector((state) => state.category)

  const [data,setData]=useState([])
 const titleInptRef =useRef()
 const pathInptRef =useRef()
console.log('categoryItems', categoryItems)

 function addproductHandler(event) {
  event.preventDefault()
    const title =titleInptRef.current.value
    const path =pathInptRef.current.value
  fetch('/api/categories',{
    method: "POST",
    body: JSON.stringify({title ,path}),
    headers: {
        'Content-Type': 'application/json'
    }
})

 }
  function editproductHandler() {
    const title =titleInptRef.current.value
    const path =pathInptRef.current.value
  fetch('/api/categories/6513f9ac817480844df324b2',{
    method: "PUT",
    body: JSON.stringify({title ,path}),
    headers: {
        'Content-Type': 'application/json'
    }
})
 } 
 async function deleteProductHandler(e,id) {
    e.persist()
    console.log('id :>> ', id);
    axios.delete(`/api/categories`,{params:id})

  //  await fetch(`/api/categories`,{
  //   method: "DELETE",
  //   headers: {
  //       'Content-Type': 'application/json'
  //   }
// })
 }

 async function handleShow (){
//  const response = await fetch('/api/categories')
 const response = await axios.get('/api/categories}')

 const responseData=await response.data
 setData(responseData)
 console.log('data',responseData)
 }

  return (
    <Layout title="firstPage">
      <Link href={"https://flaviocopes.com/nextjs-open-link-new-window/"}>
  <a target="_blank">Click this link</a>
</Link>
     <form onSubmit={addproductHandler}>
      <label> title : </label>
      <input ref={titleInptRef} type="text" placeholder='title' />
      <label> path : </label>
      <input ref={pathInptRef} type="text" placeholder='path' />
      <Button type="submit">add</Button>
     </form>

     <button onClick={()=>handleShow()}>show</button>
     <br/>
     {data?.map((item)=><div key={item._id}>{item.title} == {item._id}
     <button onClick={()=>editproductHandler()}>edit</button>
     <br/>
     
     <button onClick={(e)=>deleteProductHandler(e , item._id)}>deleete</button>
     </div>)}
     <button onClick={()=>editproductHandler()}>edit</button>
     <br/>
    </Layout>
  );
}

Home.auth=true

export default Home
